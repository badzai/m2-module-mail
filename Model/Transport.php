<?php
/**
 * Custom SMTP, SendGrid SMTP & API
 *
 * @category    Badzai
 * @package     Badzai_Mail
 * @author      ivica@badzai.com
 * @copyright   Badzai
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Badzai\Mail\Model;

class Transport implements \Magento\Framework\Mail\TransportInterface
{
    protected $transport;
    protected $message;

    /**
     * Transport constructor.
     * @param TransportFactory $transportFactory
     * @param \Magento\Framework\Mail\MessageInterface $message
     */
    public function __construct(
        \Badzai\Mail\Model\TransportFactory $transportFactory,
        \Magento\Framework\Mail\MessageInterface $message
    )
    {
        $this->message = $message;
        $this->transport = $transportFactory->create(['message' => $this->message]);
    }

    /**
     * @throws \Magento\Framework\Exception\MailException
     */
    public function sendMessage()
    {
        try {
            $this->transport->send($this->message->getMessage());
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\MailException(new \Magento\Framework\Phrase($e->getMessage()), $e);
        }
    }
}