<?php
/**
 * Custom SMTP, SendGrid SMTP & API
 *
 * @category    Badzai
 * @package     Badzai_Mail
 * @author      ivica@badzai.com
 * @copyright   Badzai
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Badzai\Mail\Model\Transport;

class SendGridApi extends \SendGrid
{
    /**
     * @param \Badzai\Mail\Model\Message\SendGridApi $message
     * @return mixed
     */
    public function send($message)
    {
        return $this->client->mail()->send()->post($message->getSendGridMessage());
    }
}