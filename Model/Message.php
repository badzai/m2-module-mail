<?php
/**
 * Custom SMTP, SendGrid SMTP & API
 *
 * @category    Badzai
 * @package     Badzai_Mail
 * @author      ivica@badzai.com
 * @copyright   Badzai
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Badzai\Mail\Model;

use Magento\Framework\Mail\MessageInterface;

class Message implements MessageInterface
{
    protected $scopeConfig;
    protected $storeScope;
    protected $fullMethodName;
    protected $methodNamespace;
    protected $method;
    protected $message;
    protected $decryptor;

    /**
     * Message constructor.
     * @param MessageFactory $messageFactory
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Config\Model\Config\Backend\Encrypted $decryptor
     */
    public function __construct(
        \Badzai\Mail\Model\MessageFactory $messageFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Config\Model\Config\Backend\Encrypted $decryptor
    )
    {
        $this->scopeConfig = $scopeConfig;
        $this->storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $this->fullMethodName = $this->scopeConfig->getValue('badzai_mail/selection/method', $this->storeScope );
        $parts = explode('_', $this->fullMethodName);
        $this->methodNamespace = $parts[0];
        $this->method = $parts[1];
        $this->message = $messageFactory->create(['method' => $this->fullMethodName]);
        $this->decryptor = $decryptor;
    }

    /**
     * @param string $subject
     * @return $this
     */
    public function setSubject($subject)
    {
        $this->message->setSubject($subject);
        return $this;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        $this->message->getSubject();
    }

    /**
     * @param string $body
     * @return $this
     */
    public function setBody($body)
    {
        $this->message->setBody($body);
        return $this;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        $this->message->getBody();
    }

    /**
     * @param array|string $fromAddress
     * @param string $name
     * @return $this
     */
    public function setFrom($fromAddress, $name='')
    {
        $this->message->setFrom($fromAddress, $name);
        return $this;
    }

    /**
     * @param array|string $toAddress
     * @param string $name
     * @return $this
     */
    public function addTo($toAddress, $name='')
    {
        $this->message->addTo($toAddress, $name);
        return $this;
    }

    /**
     * @param array|string $ccAddress
     * @param string $name
     * @return $this
     */
    public function addCc($ccAddress, $name='')
    {
        $this->message->addCc($ccAddress, $name);
        return $this;
    }

    /**
     * @param array|string $bccAddress
     * @return $this
     */
    public function addBcc($bccAddress)
    {
        $this->message->addBcc($bccAddress);
        return $this;
    }

    /**
     * @param array|string $replyToAddress
     * @param string $name
     * @return $this
     */
    public function setReplyTo($replyToAddress, $name='')
    {
        $this->message->setReplyTo($replyToAddress, $name='');
        return $this;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setMessageType($type)
    {
        $this->message->setMessageType($type);
        return $this;
    }

    /**
     * @return MessageInterface
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return array
     */
    public function getConfigurationArray()
    {
        switch($this->method){
            case "api":
                $apiKey = $this->decryptor->processValue($this->getSystemConfigValue($this->methodNamespace, 'api'));
                return ['method' => $this->fullMethodName, 'config_data' => ['apiKey' => $apiKey]];
                break;
            case "smtp":
            default:
                return [
                    'method' => $this->fullMethodName,
                    'config_data' => [
                        'host' => $this->getSystemConfigValue($this->methodNamespace, 'host'),
                        'config' => $this->getSmtpConfig()
                    ]
                ];
                break;
        }
    }

    /**
     * @param string $prefix
     * @param string $keyword
     * @param bool $sufix
     * @return mixed
     */
    protected function getSystemConfigValue($prefix, $keyword, $sufix = false)
    {
        $fullPath = 'badzai_mail/selection/' . implode('_', array_filter([$prefix, $keyword, $sufix]));
        return $this->scopeConfig->getValue($fullPath, $this->storeScope );
    }

    protected function getSmtpPort($ssl)
    {
        switch($this->methodNamespace){
            case "sendgrid":
                return $this->getSystemConfigValue($this->methodNamespace, 'port', $ssl);
                break;
            default:
                return $this->getSystemConfigValue($this->methodNamespace, 'port');
                break;
        }
    }

    /**
     * @return array
     */
    protected function  getSmtpConfig()
    {
        $ssl = $this->getSystemConfigValue($this->methodNamespace, 'security');
        $port = $this->getSmtpPort($ssl);
        $config = [
            'ssl' => $ssl,
            'port' => $port,
            'auth' => $this->getSystemConfigValue($this->methodNamespace, 'auth'),
            'username' => $this->getSystemConfigValue($this->methodNamespace, 'username'),
            'password' => $this->decryptor->processValue($this->getSystemConfigValue($this->methodNamespace, 'password'))
        ];
        return $config;
    }
}