<?php
/**
 * Custom SMTP, SendGrid SMTP & API
 *
 * @category    Badzai
 * @package     Badzai_Mail
 * @author      ivica@badzai.com
 * @copyright   Badzai
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Badzai\Mail\Model\Source\System;

class SendGridTlsPortConfig implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {

        return [
            ['value' => 587, 'label' => __('587')],
            ['value' => 25, 'label' => __('25')],
            ['value' => 2525, 'label' => __('2525')]
        ];
    }
}