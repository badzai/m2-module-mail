<?php
/**
 * Custom SMTP, SendGrid SMTP & API
 *
 * @category    Badzai
 * @package     Badzai_Mail
 * @author      ivica@badzai.com
 * @copyright   Badzai
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Badzai\Mail\Model\Message;

class SendGridApi implements \Magento\Framework\Mail\MessageInterface
{
    protected $sendGridMail;
    protected $sendGridEmailFactory;
    protected $sendGridContent;
    protected $sendGridPersonalization;

    /**
     * SendGridApi constructor.
     * @param \SendGrid\Mail $sendGridMail
     * @param \SendGrid\EmailFactory $sendGridEmailFactory
     * @param \SendGrid\Content $sendGridContent
     * @param \SendGrid\Personalization $sendGridPersonalization
     */
    public function __construct(
        \SendGrid\Mail $sendGridMail,
        \SendGrid\EmailFactory $sendGridEmailFactory,
        \SendGrid\Content $sendGridContent,
        \SendGrid\Personalization $sendGridPersonalization
    )
    {
        $this->sendGridMail = $sendGridMail;
        $this->sendGridEmailFactory = $sendGridEmailFactory;
        $this->sendGridContent = $sendGridContent;
        $this->sendGridPersonalization = $sendGridPersonalization;
    }

    /**
     * Set message subject
     *
     * @param string $subject
     * @return $this
     */
    public function setSubject($subject)
    {
        $this->sendGridPersonalization->setSubject($subject);
    }

    /**
     * Get message subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->sendGridPersonalization->getSubject();
    }

    /**
     * Set message body
     *
     * @param mixed $body
     * @return $this
     */
    public function setBody($body)
    {
        $this->sendGridContent->setValue($body);
    }

    /**
     * Get message body
     *
     * @return mixed
     */
    public function getBody()
    {
        return $this->sendGridContent->getValue();
    }

    /**
     * Set from address
     *
     * @param string|array $fromAddress
     * @param string $name
     * @return $this
     */
    public function setFrom($fromAddress, $name='')
    {
        $this->sendGridMail->setFrom($this->getSendGridEmail($fromAddress, $name));
    }

    /**
     * Add to address
     *
     * @param string|array $toAddress
     * @param string $name
     * @return $this
     */
    public function addTo($toAddress, $name='')
    {
        $this->addEmail('addTo', $toAddress, $name);
    }

    /**
     * Add cc address
     *
     * @param string|array $ccAddress
     * @param string $name
     * @return $this
     */
    public function addCc($ccAddress, $name='')
    {
        $this->addEmail('addCc', $ccAddress, $name);
    }

    /**
     * Add bcc address
     *
     * @param string|array $bccAddress
     * @return $this
     */
    public function addBcc($bccAddress)
    {
        $this->addEmail('addBcc', $bccAddress);
    }

    /**
     * Set reply-to address
     *
     * @param string|array $replyToAddress
     * @return $this
     */
    public function setReplyTo($replyToAddress)
    {
        $this->sendGridMail->setReplyTo($this->getSendGridEmail($replyToAddress));
    }

    /**
     * Set message type
     *
     * @param string $type
     * @return $this
     */
    public function setMessageType($type)
    {
        $this->sendGridContent->setType($type);
    }

    /**
     * @param string $name
     * @param string $email
     * @return \SendGrid\Email
     */
    protected function getSendGridEmail($email, $name='')
    {
        $emailObject = $this->sendGridEmailFactory->create();
        $emailObject->setEmail($email);
        $emailObject->setName($name);
        return $emailObject;
    }

    /**
     * @param string $function
     * @param string $email
     * @param string $name
     */
    protected function addEmail($function, $email, $name='')
    {
        if (!is_array($email)) {
            $email = array($name => $email);
        }

        foreach ($email as $n => $recipient) {
            $this->sendGridPersonalization->$function($this->getSendGridEmail($recipient, $n));
        }
    }

    /**
     * @return \SendGrid\Mail
     */
    public function getSendGridMessage()
    {
        $this->sendGridMail->addContent($this->sendGridContent);
        $this->sendGridMail->addPersonalization($this->sendGridPersonalization);
        return $this->sendGridMail;
    }
}