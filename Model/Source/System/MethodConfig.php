<?php
/**
 * Custom SMTP, SendGrid SMTP & API
 *
 * @category    Badzai
 * @package     Badzai_Mail
 * @author      ivica@badzai.com
 * @copyright   Badzai
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Badzai\Mail\Model\Source\System;

class MethodConfig implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {

        return [
            ['value' => 'custom_smtp', 'label' => __('Custom Smtp')],
            ['value' => 'sendgrid_smtp', 'label' => __('SendGrid Smtp')],
            ['value' => 'sendgrid_api', 'label' => __('Sendgrid API')],
        ];
    }
}