## Magento 2 SendGrid API & Smtp

Configure Magento 2 to send transactional emails using SendGrid Smtp, SendGrid API or any other custom Smtp

### Composer installation
```
composer config repositories.badzai/m2-module-mail vcs git@bitbucket.org:badzai/m2-module-mail.git
```

```
composer require badzai/m2-module-mail:dev-master#v1.0.1
```

### Manual installation

Download and unzip to {Magento root}/app/code/Badzai/Mail


### After install
Don't forget to run ```magento module:enable Badzai_Mail``` & ```magento setup:upgrade``` after installation

### Dependencies

[SendGrid Web API v3 library](https://github.com/sendgrid/sendgrid-php) is used for Magento 2 Sengrid API capabilities


### Usage

Stores -> Configuration -> Badzai