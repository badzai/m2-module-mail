<?php
/**
 * Custom SMTP, SendGrid SMTP & API
 *
 * @category    Badzai
 * @package     Badzai_Mail
 * @author      ivica@badzai.com
 * @copyright   Badzai
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Badzai\Mail\Model\Source\System;

class SendGridSecurityProtocol extends SecurityProtocol
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        $ret = parent::toOptionArray();
        unset($ret[0]);
        return $ret;
    }
}