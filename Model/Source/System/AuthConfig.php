<?php
/**
 * Custom SMTP, SendGrid SMTP & API
 *
 * @category    Badzai
 * @package     Badzai_Mail
 * @author      ivica@badzai.com
 * @copyright   Badzai
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
namespace Badzai\Mail\Model\Source\System;

class AuthConfig implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {

        return [
            ['value' => 'none', 'label' => __('None')],
            ['value' => 'login', 'label' => __('Login')],
            ['value' => 'plain', 'label' => __('Plain')],
            ['value' => 'crammd5', 'label' => __('CRAM-MD5')]
        ];
    }
}