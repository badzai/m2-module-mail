<?php
/**
 * Custom SMTP, SendGrid SMTP & API
 *
 * @category    Badzai
 * @package     Badzai_Mail
 * @author      ivica@badzai.com
 * @copyright   Badzai
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

namespace Badzai\Mail\Model;


class TransportFactory
{
    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager = null;

    /**
     * Instance name to create
     *
     * @var string
     */
    protected $_instanceName = null;

    /**
     * Factory constructor
     *
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param string $instanceName
     */
    public function __construct(\Magento\Framework\ObjectManagerInterface $objectManager, $instanceName = '\\Badzai\Mail\\Model\\Transport\\Smtp')
    {
        $this->_objectManager = $objectManager;
        $this->_instanceName = $instanceName;
    }

    /**
     * Create class instance with specified parameters
     *
     * @param array $data
     * @return \Magento\Framework\Mail\TransportInterface
     */
    public function create(array $data = array())
    {
        $messageData = $data['message']->getConfigurationArray();
        switch ($messageData['method']){
            case 'sendgrid_api':
                return $this->_objectManager->create('\\Badzai\Mail\\Model\\Transport\\SendGridApi', $messageData['config_data']);
            break;
            default:
                return $this->_objectManager->create($this->_instanceName, $messageData['config_data']);
            break;
        }
    }
}
